<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post('/user/register', 'UserController@registerUser');
Route::post('/user/login', 'UserController@loginUser');
Route::post('/user/import', 'UserController@importUsers');
Route::post('email/send', 'UserEmailController@index');
Route::get('/user/export', 'UserController@exportUsers');
Route::get('/user/fetchusers', 'UserController@fetchAllUsers');
Route::get('/user/profile', 'UserController@userProfile')
            ->middleware('validateToken', 'validateAPIKey');
Route::put('user/update/{id}', 'UserController@updateUserDetails');
Route::patch('/user/restore/{id}', 'UserController@restoreUser');
Route::delete('user/delete/{id}', 'UserController@deleteUser');


Route::get('/role', 'RoleController@fetchRoles');
Route::get('/role/all', 'RoleController@fetchAllRoles');
Route::get('/role/archive', 'RoleController@fetchSoftDeleteRoles');
Route::get('/role/permission', 'RoleController@specificRole');
Route::post('/role/create', 'RoleController@createRole');
Route::put('/role/update/{id}', 'RoleController@updateRole');
Route::patch('/role/status/{id}', 'RoleController@statusRole');
Route::patch('/role/restore/{id}', 'RoleController@restoreRole');
Route::delete('/role/delete/{id}', 'RoleController@deleteRole');

// Route::get('/env', [UserController::class, 'envTest']);
