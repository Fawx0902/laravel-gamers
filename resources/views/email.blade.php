<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Email</title>
</head>
<body>
	<form method="POST" action="api/email/send">
		@csrf
		<div class="form-group">
			<label for="title">Email:</label>
			<input type="text" class="form-control" id="email" name="email">
		</div>

		<div class="form-group">
			<label for="content">Subject:</label>
			<input type="text" class="form-control" id="subject" name="subject">
		</div>

		<div class="form-group">
			<label for="content">Content:</label>
			<textarea class="form-control" id="content" name="content" rows="5"></textarea>
		</div>

		<div class="mt-2">
			<button type="submit" class="btn btn-primary">Send Email</button>
		</div>
	</form>
</body>
</html>