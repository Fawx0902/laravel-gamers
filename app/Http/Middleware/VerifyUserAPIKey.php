<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class VerifyUserAPIKey
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request -> header('x-api-key')) {
            if ($request -> header('x-api-key') !== env('API_KEY')) {
                return response() -> json(['Message' => 'Invalid Access Key.'], 400);
            } else {
                return $next($request);
            }
        }

        return response() -> json(['Error' => 'Unauthorized'], 401);
    }
}
