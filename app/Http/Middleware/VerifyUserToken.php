<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use ReallySimpleJWT\Token;
use Illuminate\Support\Facades\DB;
use App\Models\User;

class VerifyUserToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
       if ($request -> header('Authorization')) {
            $authorizationHeader = $request -> header('Authorization');
            $token = str_replace('Bearer ', '', $authorizationHeader);
            $secret = env('ACCESS_TOKEN');
            
            if (Token::validate($token, $secret)) {
                $user = User::find(Token::getPayload($token, $secret)['user_id']);

                if (!$user){
                    return response() -> json([
                        'Message' => 'User cannot be found.',
                    ], 404);
                }

                $userPermission = DB::table('roles')
                ->where('role', $user->role)
                ->first('permission');

                if ($userPermission->permission) {
                    $userPermission->permission = unserialize($userPermission->permission);
                } else {
                    $userPermission->permission = [];
                }

                $request -> merge([
                    'user' => [
                        'name'=> $user -> name,
                        'email' => $user -> email,
                        'role' => $user -> role,
                        'permission' => $userPermission->permission,
                    ]
                ]);

                return $next($request);

            } else {
                return response() -> json([
                    'Message' => 'Invalid Authorization Token..',
                ], 400);
            }
       }

       return response()->json(['Error' => 'Unauthorized'], 401);
    }
}
