<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Http\Requests\RegisterUserRequest;
use App\Http\Requests\LoginUserRequest;
use App\Imports\ImportUser;
use App\Exports\ExportUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use ReallySimpleJWT\Token;
use Maatwebsite\Excel\Facades\Excel;
use Throwable;

class UserController extends Controller
{
    public function registerUser(RegisterUserRequest $request)
    {
        // Logic handled in RegisterUserRequest (Http/Request)
        $request->validated();

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'role' => empty($request->role) ? 'Employee' : $request->role
        ]);

        // Format: Token::create(id, AccessToken, Expiry Date, localhost)
        $token = Token::create(
            $user -> id,
            env('ACCESS_TOKEN'),
            time() + 3600,
            'localhost'
        );

        // Make sure response objects is always small caps for easier implementation in frontend
        return response()->json([
            'message' => 'User created successfully!',
            'status' => "Success",
            'code' => 201,
            'accessToken' => $token,
            'user' => [
                'name'=> $user -> name,
                'email' => $user -> email,
                'role' => $user -> role,
            ]
        ], 201);
    }

    public function loginUser(LoginUserRequest $request)
    {
            // Logic handled in LoginrUserRequest (Http/Request)
            $request->validated();

            // If Auth fails to attempt in finding the exact details of email and password in db
            if (!Auth::attempt($request -> only('email', 'password'))) {
                $user = User::where('email', $request -> input('email'))->first();

                if (!$user){
                    return response() -> json([
                        'message' => 'Details do not exist in the database.',
                    ], 401);
                } else {
                    return response() -> json([
                        'message' => 'Incorrect Credentials.',
                    ], 401);
                }
                
            } else {
                $token = Token::create(
                    Auth::User()->id,
                    env('ACCESS_TOKEN'),
                    time() + 3600,
                    'localhost'
                );

                return response()->json([
                    'message' => 'User Login!',
                    'accessToken' => $token,
                    'user' => [
                        'name' => Auth::User()->name,
                        'email' => Auth::User()->email,
                        'role' => Auth::User()->role,
                    ],
                ], 200)
                    ->header('Content-Type', 'application/json')
                    ->header('Authorization', 'Bearer '.$token);
            }
    }

    // This function has middleware of verifying user token and provided API key
    public function userProfile(Request $request){
        $user = $request -> user; 
        return response() -> json([
            'user' => $user,
        ], 200);
    }

    // Fetch all Users
    public function fetchAllUsers(){
        $users = User::withTrashed()->get();
        return response()->json([
            'status' => "Users successfully retrieved!",
            'users' => $users,
        ], 200);
    }

    public function updateUserDetails(Request $request, $id){
        try {
            $validateUser = Validator::make($request->all(), [
                'name' => 'required',
                'email' => [
                    'required',
                    'email',
                    Rule::unique('users')->ignore($id),
                ],
                'role' => 'required',
            ]);

            if($validateUser->fails()){
                return response()->json([
                    'message' => $validateUser->errors()->all(),
                    'status' => 'Unprocessable Entity!',
                    'code' => 422 
                ], 422);
            }

            $updateUser = User::findOrFail($id);

            $updateUser->update([
                'name' => $request->name,
                'email' => $request->email,
                'role' => $request->role,
            ]);

            return response()->json([
                'status' => 'User updated successfully!',
                'updatedUser' => $updateUser,
            ], 200);

        } catch (Throwable $error){
            return response()->json($error->getMessage(), 422);
        }
    }

    public function deleteUser($id){
        $user = User::findOrFail($id);
        $user->delete();
        return response()->json([
            'status' => 'User soft-deleted successfully!'
        ], 200);
    }

    public function restoreUser($id)
    {
        $user = User::withTrashed()->find($id);

        if (!$user || !$user->trashed()){
            return response()->json([
                'status' => 'User cannot be found in the void..?',
            ], 404);
        }

        $user->restore();

        return response()->json([
            'status' => 'User is restored successfully!',
        ], 200);
    }

    public function importUsers(Request $request) {
        $request->validate([
            'file' => 'required|file|mimes:xls,xlsx,csv,txt',
        ]);
        Excel::import(new ImportUser, $request->file('file')->store('files'));
        
        return response()->json([
            'status' => 'Users is imported successfully!',
        ], 200);
    }

    public function exportUsers(){
        return Excel::download(new ExportUser, 'userRecords.xlsx');
    }

    // Debugging Function
    public function envTest(){
    $appName = env('APP_NAME');
    $dbConnection = env('API_KEY');
    
    return response()->json([
        'Message' => $appName,
        'Status' => $dbConnection,
        'Code' => 201
    ], 201);
    }
}
