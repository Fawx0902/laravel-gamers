<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Http\Requests\CreateRoleRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use Throwable;

class RoleController extends Controller
{
    public function createRole(CreateRoleRequest $request){
        $request->validated();

        $role = Role::create([
            'role' => $request->role,
            'description' => $request->description,
            'permission' => serialize($request->permission),
            'isActive' => $request->isActive,
        ]);

        return response()->json([
            'message' => 'Role created successfully!',
            'status' => "Success",
            'code' => 201,
            'role' => [
                'role' => $role->role,
                'description' => $role->description,
                'permission' => $role->permission,
                'isActive' => $role->isActive,
            ]
        ], 201);
    }

    public function fetchRoles(){
        $roles = Role::all();
        return response()->json([
            'status' => "Roles successfully retrieved!",
            'roles' => $roles,
        ], 200);
    }

    public function fetchAllRoles(){
        $roles = Role::withTrashed()->get();
        return response()->json([
            'status' => "Roles successfully retrieved!",
            'roles' => $roles,
        ], 200);
    }

    public function fetchSoftDeleteRoles(){
        $roles = Role::onlyTrashed()->get();
        return response()->json([
            'status' => "Soft-Delete roles successfully retrieved!",
            'roles' => $roles,
        ], 200);
    }

    public function specificRole(Request $request){
        $specficRole = Role::where('role', $request->role)->select('permission')->first();
        if($specficRole) {
            return response()->json([
                'status' => "Role permissions retrieved!",
                'role' => $specficRole,
            ], 200);
        } else {
            return response()->json([
                'status' => "Either Role went out for vacation or is on the naughty corner..?",
            ], 404);
        }
        
    }

    public function updateRole(Request $request, $id){
        try {
            $validateRole = Validator::make($request->all(), [
                'role' => [
                    'required',
                    'string',
                    Rule::unique('roles')->ignore($id), // Condition to prevent user from inputting duplicate role except itself (Ex. Cannot rename Admin to Employee but Admin can still stay as Admin if updating other data fields.)
                ],
                'description' => 'required|string',
                'permission' => 'required',
            ]);

            if($validateRole->fails()){
                return response()->json([
                    'message' => $validateRole->errors()->all(),
                    'status' => 'Unprocessable Entity!',
                    'code' => 422 
                ], 422);
            }

            $updateRole = Role::findOrFail($id);
            $oldRoleName = $updateRole->role;

            DB::table('users')
            ->where('role', $oldRoleName)
            ->update(['role' => $request->role]);

            $updateRole->update([
                'role' => $request->role,
                'description' => $request->description,
                'permission' => serialize($request->permission),
            ]);

            return response()->json([
                'status' => 'Role updated successfully!',
                'role' => $updateRole,
            ], 200);

        } catch (Throwable $error){
            return response()->json($error->getMessage(), 422);
        }
    }

    public function statusRole($id)
    {
        $role = Role::find($id);

        if (!$role){
            return response()->json([
                'status' => 'Role cannot be found..',
                'role' => $role,
            ], 404);
        }

        $role->isActive = !$role->isActive;
        $role->save();

        return response()->json([
            'status' => 'Role status updated successfully!',
            'role' => $role,
        ], 200);
    }

    public function restoreRole($id)
    {
        $role = Role::withTrashed()->find($id);
        if (!$role || !$role->trashed()){
            return response()->json([
                'status' => 'Role cannot be found in the void..',
                'role' => $role,
            ], 404);
        }

        $role->restore();

        return response()->json([
            'status' => 'Role is restored successfully!',
        ], 200);
    }

    public function deleteRole($id){
        $role = Role::findOrFail($id);
        $roleNameToDelete = $role->role;

        // Check if any users have the role to be deleted
        $usersWithRoleToDelete = DB::table('users')
            ->where('role', $roleNameToDelete)
            ->count();

        if ($usersWithRoleToDelete > 0) {
            // If users have the role to be deleted, update their roles to 'Employee'
            DB::table('users')
                ->where('role', $roleNameToDelete)
                ->update(['role' => 'Employee']);
        }

        $role->delete();

        return response()->json([
            'status' => 'Role soft-deleted successfully!'
        ], 200);
    }
}
