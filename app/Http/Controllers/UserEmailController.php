<?php

namespace App\Http\Controllers;

use App\Http\Requests\SendEmailRequest;
use App\Mail\SendUserMail;
use Illuminate\Support\Facades\Mail;
use Throwable;



class UserEmailController extends Controller
{
    public function index(SendEmailRequest $request){
        $request->validated();

        try {
            Mail::to($request->email)->send(new SendUserMail($request->subject, $request->content));
            return response()->json([
                'message' => 'Email sent successfully!',
            ]);

            return view('success');
        } catch (Throwable $e) {
            return response()->json([
                'message' => 'Something went wrong...',
                'error' => $e,
            ]);
        }
    }
}
