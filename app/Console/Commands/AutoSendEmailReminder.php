<?php

namespace App\Console\Commands;

use App\Jobs\EmailReminder;
use Illuminate\Console\Command;

class AutoSendEmailReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:auto-send-email-reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remind the user to brush their teeth.';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $subject = "Daily Reminder";
        $content = "Brush Brush Brush, three times a day. Brush with Colgate!";
        dispatch(new EmailReminder("vonlycaon@victoria.co", $subject, $content));
    }
}
